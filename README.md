
### Generate previews from the PDFs
Run:

   ``genpreview [-v|--verbose] [-maxdepth <LEVEL>] [--no-shadow] DIRECTORY``

to generated HTML preview pages in DIRECTORY and all sub directories up to <LEVEL> levels (default: 1).

If no PDFs exist yet, the available documents have to be converted to PDF first.
To do that, mount DIR locally with:

   ``sshfs <USER>@<HOST>://THE/DIR/ mnt/``

Then run for example:

   ``for ODT in $(find DIR -name "*.odt") ; do
       libreoffice --headless --convert-to pdf $ODT --outdir $(dirname $ODT) ;
     done``
